/**
 * Candidate class.
 */
export class Candidate {

    id: number = null;
    name: string = '';
    lastName: string = '';
    gender: string = '';
    birthDate: string = '';
    country: string = '';
    state: string = '';
    city: string = '';
    address: string = '';
    phoneCode: number = null;
    phone: number = null;
    
}