import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { CandidateRegisterPage } from '../candidate-pages/candidate-register/candidate-register';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  candidateRegisterRoot = CandidateRegisterPage;

  constructor() {

  }
}
