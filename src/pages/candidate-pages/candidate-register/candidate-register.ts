import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Candidate } from '../../../classes/Candidate';
import { CandidateRegisterTwoPage } from './candidate-register-two';

@Component({
    selector: 'page-candidate-register',
    templateUrl: 'candidate-register.html'
})
export class CandidateRegisterPage {

    public candidate: Candidate;
    public name: string = null;
    public lastName: string = null;
    public gender: string = null;
    public birthDate: string = null;

    constructor(public navCtrl: NavController) {

    }

    public logForm(): void {
        console.log(this.name + this.lastName + this.gender + this.birthDate);
        //console.log(this.candidate);
    }

    public goToRegisterTwo(): void {
        this.navCtrl.push(CandidateRegisterTwoPage, {
            name: this.name,
            lastName: this.lastName,
            gender: this.gender,
            birthDate: this.birthDate
          });
    }

}
