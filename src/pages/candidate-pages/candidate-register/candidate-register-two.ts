import { Component } from '@angular/core';
import { NavController, AlertController, NavParams } from 'ionic-angular';
import { Candidate } from '../../../classes/Candidate';

@Component({
    selector: 'page-candidate-register-two',
    templateUrl: 'candidate-register-two.html'
})
export class CandidateRegisterTwoPage {

    public candidate: Candidate;
    public name: string = null;
    public lastName: string = null;
    public gender: string = null;
    public birthDate: string = null;
    public country: string = null;
    public state: string = null;
    public city: string = null;
    public address: string = null;
    public phoneCode: number = null;
    public phone: number = null;

    constructor(public navCtrl: NavController, private alertCtrl: AlertController,
        private navParams: NavParams) {
        this.name = navParams.get('name');
        this.lastName = navParams.get('lastName');
        this.gender = navParams.get('gender');
        this.birthDate = navParams.get('birthDate');
    }

    public logForm(): void {
        console.log(this.country + this.state + this.city + this.address + this.phoneCode + this.phone + this.phone);
    }

    public openFilters(): void {
        let alert = this.alertCtrl.create({
            title: 'Datos del candidato',
            message: 'nombre: '+this.name+', apellido: '+this.lastName
            +', género: '+this.gender+', fecha nacimiento: '+this.birthDate
            +', país: '+this.country+', estado: '+this.state+', ciudad: '+this.city+', dirección: '+this.address+
            ', código teléfono: '+this.phoneCode+', teléfono: '+this.phone,
            buttons: ['OK']
        });
    
        alert.present();
    }

}
